#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# 1. Follow the install instructions for pywikibot:
# https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Copy this file into the pywikibot "scripts" directory


import pywikibot
import argparse
import json
import csv

from urllib.parse import quote
from datetime import datetime
from pywikibot.comms import http


class ClientHintsPermissionsComparison(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-r', '--revids', nargs='*', default=False)
        parser.add_argument('-d', '--deleted-revids', nargs='*', default=False)
        parser.add_argument('-i', '--ip', default=False)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        headers = {"Content-Type": "application/json"}
        if self.options.ip:
            headers['X-Forwarded-For'] = self.options.ip
            username = self.options.ip
            self.site.logout()
        else:
            username = self.site.username()
            self.site.login()

        clienthints = {"architecture":"",
                       "bitness":"64",
                       "brands":[{"brand":"Not.A/Brand","version":"8"},{"brand":"Chromium","version":"114"}],
                       "fullVersionList":[{"brand":"Not.A/Brand","version":"8.0.0.0"},{"brand":"Chromium","version":"114.0.5735.198"}],
                       "mobile":False,
                       "model":"",
                       "platform":"Linux",
                       "platformVersion":"5.10.0"}
        fields = ["revid", "user", "userid", "userhidden", "suppressed", "deleted", "httpCode", "httpReason", "errorKey", "messageTranslations", "value", "anon", "parentid"]
        rows = []

        if self.options.revids:
            revisions = self.options.revids
        elif self.options.deleted_revids:
            revisions = self.options.deleted_revids
        else:
            pywikibot.output("Either -r or -d must be passed")
            quit()

        revid_batches = []
        for start in list(range(0, len(revisions), 50)):
            finish = start + 50
            revid_batches.append(revisions[start:finish])

        api_responses = []
        for revid_batch in revid_batches:
            if self.options.revids:
                api_request = self.site.simple_request(action='query', prop='revisions',
                                                       revids="|".join(revid_batch),
                                                       rvprop="ids|user|userid")
                api_response = api_request.submit()
                api_responses.extend(list(api_response['query']['pages'].values())[0]['revisions'])
            else:
                api_request = self.site.simple_request(action='query', prop='deletedrevisions',
                                                       revids="|".join(revid_batch), drvlimit=50,
                                                       drvprop="ids|user|userid")
                api_response = api_request.submit()
                if 'pages' in api_response['query']:
                    api_responses.extend(list(api_response['query']['pages'].values())[0]['deletedrevisions'])
                else:
                    api_responses.extend([{'revid': rev, 'deleted': True} for rev in revid_batch])

        for rev in api_responses:
            for key in rev.keys():
                if key not in fields:
                    fields.append(key)
            row = rev
            rest_response = http.session.post("{}://{}{}/rest.php/checkuser/v0/useragent-clienthints/revision/{}".format(self.site.protocol(), self.site.family.langs[self.site.code],
                                                                                                                         self.site.scriptpath(), rev['revid']), json.dumps(clienthints), headers=headers).json()
            for key in rest_response.keys():
                if key not in fields:
                    fields.append(key)
            row.update(rest_response)
            rows.append(row)

        with open("clienthints_permissions_comparisons_{}_{}_{}.csv".format(self.site.family.langs[self.site.code], quote(username), datetime.now().strftime("%Y-%m-%dT%H:%M:%S")), 'a', newline='') as csvfile:
            dictcsv = csv.DictWriter(csvfile, fields, restval=False, extrasaction="ignore")
            dictcsv.writeheader()
            dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = ClientHintsPermissionsComparison(*args)
    app.run()


if __name__ == '__main__':
    main()
