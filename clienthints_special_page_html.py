#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# 1. Follow the install instructions for pywikibot:
# https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Copy this file into the pywikibot "scripts" directory


import argparse
import pywikibot
import re

from pywikibot.comms import http
from bs4 import BeautifulSoup


class ClientHintsSpecialPageHtml(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=10)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        pages = [
            "Special:MostLinkedPages",
            "Special:MostTranscludedPages",
            "Special:MostCategories",
            "Special:MostInterwikis",
            "Special:MostRevisions",
            "Special:Book",
            "Special:ChangeContentModel",
            "Special:PageLanguage",
            "Special:CiteThisPage",
            "Special:ComparePages",
            "Special:Export",
            "Special:Import",
            "Special:Nuke",
            "Special:PageAssessments",
            "Special:UrlShortener",
            "Special:Undelete",
            "Special:WhatLinksHere",
            "Special:BlockedExternalDomains",
            "Special:LandingCheck",
            "Special:ActiveLanguages",
            "Special:Translations",
            "Special:ExportTranslations",
            "Special:LanguageStats",
            "Special:MessageGroupStats",
            "Special:PageTranslation",
            "Special:SearchTranslations",
            "Special:Translate",
            "Special:TranslationStats",
            "Special:TranslatorSignup",
            "Special:EditGrowthConfig",
            "Special:Impact",
            "Special:ManageMentors",
            "Special:MentorDashboard",
            "Special:NewcomerTasksInfo",
            "Special:CancelEventRegistration",
            "Special:DeleteEventRegistration",
            "Special:EditEventRegistration",
            "Special:EnableEventRegistration",
            "Special:EventDetails",
            "Special:RegisterForEvent",
            "Special:MyEvents",
            "Special:AboutTopic",
            "Special:ContentTranslationStats",
            "Special:Contribute",
            "Special:CreateMassMessageList",
            "Special:DiscussionToolsDebug",
            "Special:FindComment",
            "Special:GlobalRenameProgress",
            "Special:NewsFeed",
            "Special:MathWikibase",
            "Special:MathStatus",
            "Special:Newsletters",
            "Special:ORESModels",
            "Special:SecurePoll",
            "Special:ContentTranslation"
        ]
        for page in pages:
            html = http.session.get("{}://{}/wiki/{}".format(self.site.protocol(), self.site.family.langs[self.site.code], page)).text
            soup = BeautifulSoup(html, 'html.parser')
            script = soup.find("script")
            if script:
                num = len(script.find_all(string=re.compile("ext.checkUser.clientHints")))
            else:
                num = 0
            pywikibot.output("{}{}: {}{}".format('\033[92m' if num == 1 else '\033[91m', page, num, '\033[0m'))


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = ClientHintsSpecialPageHtml(*args)
    app.run()


if __name__ == '__main__':
    main()
