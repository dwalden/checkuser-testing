# checkuser-testing

Scripts I have written for testing checkuser.

How to use these scripts
========================

1. Setup pywikibot using the instructions here https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
2. Copy the scripts in this repository into the `scripts/` directory in pywikibot

Some of the scripts will have documentation in the source file itself. Others will not, but hopefully you can work out the supported parameters by reading the `set_options()` function.

Testing local wikis
====================

To be able to use pywikibot against your local docker wiki, you need to generate them:
```
python3 pwb.py generate_family_file http://localhost:8080 dockerwiki n
```

Add this to `user-config.py` ('Admin' is the default docker admin user):
```
usernames['dockerwiki']['en'] = u'Admin'
```

Then to run any of the scripts against your local docker wiki you can run: `python3 pwb.py <script> <arguments> -lang:en -family:dockerwiki`.
