#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# 1. Follow the install instructions for pywikibot:
# https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Copy this file into the pywikibot "scripts" directory


import pywikibot
import argparse
import json
import time
import random
import uuid
import pprint
import sqlite3

from pywikibot.comms import http
from pywikibot.data.mysql import mysql_query
from hypothesis import settings, given, HealthCheck, Phase
import hypothesis.strategies as st

my_args = pywikibot.handle_args()
parser = argparse.ArgumentParser()
parser.add_argument('-n', '--num', type=int, default=10)
parser.add_argument('-i', '--ip', default=False)
parser.add_argument('-t', '--trigger', action='store_true')
parser.add_argument('-w', '--wait', action='store_true')
parser.add_argument('-m', '--max-wait', type=int, default=1)
parser.add_argument('-s', '--sleep', type=float, default=False)
parser.add_argument('-r', '--reps', type=int, default=2)
parser.add_argument('-d', '--db', action='store_true')
parser.add_argument('--sqlite', default=False)
options = parser.parse_args(my_args)

errors = {}


@st.composite
def clienthints(draw):
    test_atom = st.one_of(st.booleans(), st.text(), st.characters(), st.integers(), st.none())
    test_atom_list = st.one_of(st.booleans(), st.text(), st.characters(), st.integers(), st.none(), st.lists(test_atom))
    test_atom_dict = st.one_of(st.booleans(), st.text(), st.characters(), st.integers(), st.none(), st.dictionaries(test_atom, test_atom_list))
    test_composite = st.one_of(st.booleans(), st.text(), st.characters(), st.integers(), st.none(), st.dictionaries(test_atom, st.recursive(test_atom_list, lambda x: st.dictionaries(test_atom, x), max_leaves=3)), st.lists(st.recursive(test_atom_dict, lambda x: st.lists(x), max_leaves=3)))
    brands_strat = st.one_of(test_composite,
                             st.lists(st.fixed_dictionaries({},
                                                            optional={'brand': test_composite,
                                                                      'version': test_composite})))
    # brands_strat = st.lists(st.fixed_dictionaries({'brand': test_composite,
    #                                                'version': test_composite}))

    clienthints_json = '{'
    strip = False
    for key in ['architecture','bitness','brands','fullVersionList','mobile','model','platform','platformVersion']:
        for i in range(0, random.randint(1, options.reps)):
            if key not in ['brands', 'fullVersionList']:
                clienthints_json += '"{}": {}, '.format(key, json.dumps(draw(test_composite)))
            else:
                foo = draw(brands_strat)
                # if type(foo) is list:
                    # foo.extend(foo)
                clienthints_json += '"{}": {}, '.format(key, json.dumps(foo))
            strip = True
    if strip:
        clienthints_json = clienthints_json[:-2]
    clienthints_json += '}'
    return clienthints_json


class ClientHintsEditRandomDataRepetitions(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.site = pywikibot.Site()

    @given(clienthints())
    @settings(max_examples=options.num, deadline=None, phases=[Phase.generate], suppress_health_check=[HealthCheck.too_slow, HealthCheck.large_base_example, HealthCheck.data_too_large])
    def run(self, clienthints):
        """Run the bot."""
        self.site.throttle.setDelays(0, 0, True)
        headers = {"Content-Type": "application/json"}
        if options.ip:
            headers['X-Forwarded-For'] = options.ip
            username = options.ip
            self.site.logout()
        else:
            username = self.site.username()
            self.site.login()

        pywikibot.output("===============================")

        page = pywikibot.Page(self.site, "pageName")
        page.text = str(uuid.uuid4())
        page.save("Edit comment")

        if options.sleep:
            time.sleep(options.sleep)

        if options.trigger:
            # Trigger running of purge job.
            http.session.get("{}://{}{}/index.php/Main_Page".format(self.site.protocol(),
                                                                    self.site.family.langs[self.site.code],
                                                                    self.site.scriptpath()))

            if options.wait:
                # Wait between 0 and max_wait seconds
                time.sleep(random.uniform(0, options.max_wait))

        rev = page.latest_revision_id
        rest_response = http.session.post("{}://{}{}/rest.php/checkuser/v0/useragent-clienthints/revision/{}".format(self.site.protocol(), self.site.family.langs[self.site.code],
                                                                                                                     self.site.scriptpath(), rev), clienthints, headers=headers).json()

        if 'message' in rest_response:
            if rest_response['message'] in errors:
                errors[rest_response['message']] += 1
            else:
                errors[rest_response['message']] = 1

        pywikibot.output(rev)
        pywikibot.output(clienthints)
        pywikibot.output("Try one:")
        pywikibot.output(rest_response)

        rest_response = http.session.post("{}://{}{}/rest.php/checkuser/v0/useragent-clienthints/revision/{}".format(self.site.protocol(), self.site.family.langs[self.site.code],
                                                                                                                     self.site.scriptpath(), rev), clienthints, headers=headers).json()
        if 'message' in rest_response:
            if rest_response['message'] in errors:
                errors[rest_response['message']] += 1
            else:
                errors[rest_response['message']] = 1

        pywikibot.output("Try two:")
        pywikibot.output(rest_response)

        if options.db:
            query = """
            SELECT uach_name, uach_value FROM cu_changes
            INNER JOIN cu_useragent_clienthints_map ON cuc_this_oldid=uachm_reference_id
            INNER JOIN cu_useragent_clienthints ON uachm_uach_id=uach_id
            WHERE cuc_this_oldid={};
            """.format(rev)
            if options.sqlite:
                conn = sqlite3.connect(options.sqlite)
                cursor = conn.cursor()
                cursor.execute(query)
                sqlresults = cursor.fetchall()
                conn.close()
            else:
                sqlresults = mysql_query(query, dbname="my_wiki")

            sql = []
            for result in sqlresults:
                if options.sqlite:
                    sql.append("{}_{}".format(result[0], result[1]))
                else:
                    sql.append("{}_{}".format(result[0].decode(), result[1].decode()))

            pywikibot.output(len(sql))
            pywikibot.output(sql)
            if len(sql) > 0:
                for key, value in json.loads(clienthints).items():
                    if key in ["brands", "fullVersionList"] and type(key) is list:
                        for d in value:
                            v = "{}{}{}".format("" if 'brand' not in d else d['brand'] if type(d['brand']) is not bool else int(d['brand']), " " if 'brand' in d and 'version' in d else "", "" if 'version' not in d else d['version'] if type(d['version']) is not bool else int(d['version']))
                            if v != "" and "{}_{}".format(key, v) not in sql:
                                pywikibot.output("FAIL: {} {}".format(key, v))
                    else:
                        if value is not None and "{}_{}".format(key, value if type(value) is not bool else int(value)) not in sql:
                            pywikibot.output("FAIL: {} {}".format(key, value))

        pywikibot.output("===============================")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = ClientHintsEditRandomDataRepetitions(*args)
    app.run()
    pprint.pprint(errors)


if __name__ == '__main__':
    main()
