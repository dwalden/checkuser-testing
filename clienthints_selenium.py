#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# 1. Follow the install instructions for pywikibot:
# https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Copy this file into the pywikibot "scripts" directory


import argparse
import pywikibot
import random
import string

from pywikibot.data import api
from selenium import webdriver
from pyvirtualdisplay import Display


class ClientHintsSelenium(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=10)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        display = Display(visible=0, size=(800, 600))
        display.start()
        browser = webdriver.Chrome()
        browser.get("{}://{}/wiki/Special:UserLogin".format(self.site.protocol(),
                                                            self.site.family.langs[self.site.code]))
        browser.find_element_by_id('wpName1').send_keys(self.site.username())
        login_manager = api.LoginManager(site=self.site, user=self.site.username())
        browser.find_element_by_id('wpPassword1').send_keys(login_manager.password)
        browser.find_element_by_id('wpLoginAttempt').click()
        for i in range(0, self.options.total):
            try:
                browser.get("{}://{}/wiki/Main_Page?action=edit".format(self.site.protocol(),
                                                                        self.site.family.langs[self.site.code]))
                browser.find_element_by_name('wpTextbox1').send_keys(''.join(random.choice(string.printable) for i in range(30)))
                browser.find_element_by_name('wpSave').click()
                if browser.find_element_by_id('firstHeading').text == "Main Page":
                    continue
                elif browser.find_element_by_id('firstHeading').text == "Edit conflict: Main Page":
                    browser.find_element_by_name('wpTextbox1').send_keys(''.join(random.choice(string.printable) for i in range(30)))
                    browser.find_element_by_name('wpSave').click()
            except:
                continue
        browser.quit()
        display.stop()


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = ClientHintsSelenium(*args)
    app.run()


if __name__ == '__main__':
    main()
